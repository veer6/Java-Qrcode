package com.veer.QrCode;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;

import org.apache.http.HttpEntity;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;

public class Tools {
	
	/**
	 * 根据链接下载图片
	 * 
	 * @param client
	 * @param url
	 * @param filepath
	 */
	public static void download(CloseableHttpClient client, String url, String filepath)
	{
		HttpGet request = new HttpGet(url);
        try {
            CloseableHttpResponse response = client.execute(request);
            if (HttpStatus.SC_OK == response.getStatusLine().getStatusCode()) {
                HttpEntity entity = response.getEntity();
                InputStream in = entity.getContent();
                File file = new File(filepath);
                if(!file.exists()){
                    file.createNewFile();
                  }
                OutputStream out = new FileOutputStream(file); 
                byte[] buffer = new byte[4096];
                int readLength = 0;
                while ((readLength=in.read(buffer)) > 0) {
                  byte[] bytes = new byte[readLength];
                  System.arraycopy(buffer, 0, bytes, 0, readLength);
                  out.write(bytes);
                }
                 
                out.flush();
                System.out.println("下载成功:" + filepath);
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        } finally {
            request.releaseConnection();
        }
	}
	
	/**
	 * 生成二维码
	 * 
	 * @param width
	 * @param height
	 * @param content
	 * @return
	 * @throws WriterException 
	 * @throws IOException 
	 * @throws Exception
	 */
	public static byte[] makeQrCode(int width, int height, String content) throws WriterException, IOException{
		// 二维码基本参数
		Map<EncodeHintType, Object> hints = new HashMap<EncodeHintType, Object>();
		hints.put(EncodeHintType.CHARACTER_SET, "utf-8");// 设置编码字符集utf-8
		hints.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.M);// 设置纠错等级L/M/Q/H,纠错等级越高越不易识别，当前设置等级为最高等级H
		hints.put(EncodeHintType.MARGIN, 0);// 可设置范围为0-10，但仅四个变化0 1(2) 3(4 5 6) 7(8 9 10)
		BarcodeFormat format = BarcodeFormat.QR_CODE;
		// 创建位矩阵对象
		BitMatrix bitMatrix = new MultiFormatWriter().encode(content, format, width, height, hints);
		// 位矩阵对象转流对象
		ByteArrayOutputStream os = new ByteArrayOutputStream();
		MatrixToImageWriter.writeToStream(bitMatrix, "png", os);
		return os.toByteArray();
	}
}
