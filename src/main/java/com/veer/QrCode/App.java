package com.veer.QrCode;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.util.concurrent.LinkedBlockingQueue;

import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.google.zxing.WriterException;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args ) throws InvalidFormatException, IOException, InterruptedException, WriterException
    {
        System.out.println( "程序开始运行咯!" );
        
        final LinkedBlockingQueue linkQueue = new LinkedBlockingQueue<>();
        Thread thread = new Thread(new Runnable() {
			
        	@Override
			public void run() {
				// TODO Auto-generated method stub
				try {
					System.out.println(linkQueue.take());
				} catch (Exception e) {
					
				}
			}
		});
        
        linkQueue.add(2);
        linkQueue.add(1);
        thread.start();
        
//        String excelPath = "D:\\doc.xlsx";
//        File excel = new File(excelPath);
//        if(!excel.exists()){
//        	System.out.println("文件不存在哦");
//        }
//        Workbook wb = new XSSFWorkbook(excel);
//        
//        //开始解析
//        Sheet sheet = wb.getSheetAt(1);     //读取sheet 0
//
//        int firstRowIndex = sheet.getFirstRowNum()+1;   //第一行是列名，所以不读
//        int lastRowIndex = sheet.getLastRowNum();
//        CloseableHttpClient client =   HttpClients.createDefault();
//        for(int rIndex = firstRowIndex; rIndex <= lastRowIndex; rIndex++) {   //遍历行
//            // System.out.println("rIndex: " + rIndex);
//            Row row = sheet.getRow(rIndex);
//            if (row != null) {
//                Cell cell = row.getCell(1);
//                Cell cell2 = row.getCell(2);
//                String value = "https://byf.sandpay.com/byfcard?code=" + cell2.toString() + "&cardno=" + cell.toString();
//                System.out.println(value);
//                
//                String url = "https://api.pwmqr.com/qrcode/create/?url=" + URLEncoder.encode(value) + "&down=1";
//                
//                String filepath = "E:\\software\\qrcode\\" + cell.toString() + ".png";
//                // Tools.download(client, url, filepath);
//                
//                byte[] b = Tools.makeQrCode(250, 250, value);
//                OutputStream os = new FileOutputStream(filepath);
//                os.write(b);
//                os.close();
//            }
//            Thread.sleep(1500);
//            break;
//        }
    }
}
